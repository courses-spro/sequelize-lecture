module.exports = {
  database: 'default',
  dialect: 'postgres',
  pool: 10,
  logging: (msg) => {
    console.log(
      'executing something',
      msg
    );
  },
  define: {
    timestamps: true,
    freezeTableName: true,
    underscoredAll: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    underscored: true,
  },

  password: 'test_test',
  username: 'test'
};
