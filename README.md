# Sequelize

Official docs - https://sequelize.org/master/

What is ORM - https://ru.wikipedia.org/wiki/ORM

To set up sequelize with postgres use

``` bash
npm install --save sequelize

npm install --save pg pg-hstore # Postgres
```

To set up sequelize with existing project use https://www.npmjs.com/package/sequelize-cli

Config file is in config/database.js

but by default sequelize expect it to be in __dirname + '/../config/config.json'

## Homework
Instead of pure pg use sequelize to make requests to database in you koa server.
